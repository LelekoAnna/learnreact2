import axios from "axios";

const instance = axios.create({
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    withCredentials: true,
    headers: {
        "API-KEY": 'aabd60f3-0c17-43da-a5c8-71499a49dd78'
    }
});

export const loginAPI = {
    authMe() {
        return instance.get('auth/me').then(response => response.data);
    },
    signIn(properties) {
        return instance.post('auth/login', properties).then(response => response.data);
    },
    signOut() {
        return instance.delete('auth/login').then(response => response.data);
    }
};

export const usersAPI = {
    getUsers(currentPage = 1, pageSize = 10) {
        return instance.get(`/users?page=${currentPage}&count=${pageSize}`).then(response => response.data);
    },
    isFriend(userId) {
        return instance.get('follow/' + userId).then(response => response.data);
    },
    unFollow(userId) {
        return instance.delete('follow/' + userId).then(response => response.data);
    },
    follow(userId) {
        return instance.post('follow/' + userId).then(response => response.data);
    }
};

export const profileAPI = {
    getUserProfile(userId) {
        return instance.get('profile/' + userId).then(response => response.data);
    },
    getUserStatus(userId) {
        return instance.get('profile/status/' + userId).then(response => response.data);
    },
    updateStatus(status) {
        return instance.put('profile/status', {status}).then(response => response.data);
    }
};
