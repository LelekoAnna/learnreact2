import React from 'react';
import './App.css';

import { Route, withRouter } from 'react-router-dom';

import Nav from "./components/Navbar/Nav";
import News from "./components/News/News";
import Music from "./components/Music/Music";
import Settings from "./components/Settings/Settings";
import UsersContainer from "./components/Users/UsersContainer";
import HeaderContainer from "./components/Header/HeaderContainer";
import DialogsContainer from "./components/Dialogs/DialogsContainer";
import ProfileContainer from "./components/ProfileContent/ProfileContainer";
import Login from "./components/Login/Login";
import {initializeApp} from "./redux/app-reducer";
import {connect} from "react-redux";
import {compose} from "redux";

class App extends React.Component {
    componentDidMount() {
        this.props.initializeApp()
    }

    render() {
        if (!this.props.init) {
            return <p>Wait a minute...</p>
        }
        return (
            <div className='app-wrapper'>
                <HeaderContainer />
                <Nav />
                <div className='content'>
                    <Route path='/login' component={Login} />
                    <Route path='/profile/:userId?' render={() => <ProfileContainer />} />
                    <Route path='/messages' render={() => <DialogsContainer />} />
                    <Route path='/news' component={News} />
                    <Route path='/music' component={Music} />
                    <Route path='/users' render={() => <UsersContainer />} />
                    <Route path='/settings' component={Settings} />
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => ({
        init: state.app.initialize
    }
)

const mapDispatchToProps = (dispatch) => ({
    initializeApp: () => {
        dispatch(initializeApp())
    }
});

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(App);
