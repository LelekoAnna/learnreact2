import React from "react";
import './Nav.css'
import {NavLink} from "react-router-dom";

const Nav = () => {
    return (
        <nav className='nav'>
            <ul className='nav-list'>
                <li className='nav-list__item'><NavLink to='/profile'>Profile</NavLink></li>
                <li className='nav-list__item'><NavLink to='/messages'>Messages</NavLink></li>
                <li className='nav-list__item'><NavLink to='/news'>News</NavLink></li>
                <li className='nav-list__item'><NavLink to='/music'>Music</NavLink></li>
                <li className='nav-list__item'><NavLink to='/users'>Users</NavLink></li>
                <li className='nav-list__item'><NavLink to='/settings'>Settings</NavLink></li>
            </ul>
        </nav>
    )
};

export default Nav;