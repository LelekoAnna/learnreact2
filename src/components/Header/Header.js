import React from "react";
import './Header.css'
import {NavLink} from "react-router-dom";

const Header = (props) => {
    return (
        <header className='header'>
            <img className='header__logo' alt='logo'
                 src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Logo_TV_2015.svg/1200px-Logo_TV_2015.svg.png"/>
            <div>
                {props.isAuth ?
                    <div className='header__login'>
                        <p>
                            {props.login}
                        </p>
                        <button className='header__button' onClick={props.logout}>Logout</button>
                    </div> :
                    <button className='header__button'>
                        <NavLink to={'/login'}>Login</NavLink>
                    </button>
                }
            </div>
        </header>
    )
};

export default Header;