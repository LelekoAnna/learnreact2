import React from "react";
import "./Profile.css";
import userPhoto from "../../../assets/images/default-user-image.png"
import Contacts from "./Contacts/Contacts";
import ProfileStatus from "./ProfileStatus/ProfileStatus";

const Profile = (props) => {

    return (
        <div className="profile">
            <img className="profile__photo"
                 src={props.profile.photos.small || userPhoto}
                 alt="user photo"/>
            <div className="profile__info">
                <h2>{props.profile.fullName}</h2>
                <ProfileStatus status={props.status} updateStatus={props.updateStatus}/>
                {props.profile.lookingForAJob ? <p>Looking for a job</p> : null}
                {props.profile.lookingForAJobDescription ?
                    <small>Looking for: {props.profile.lookingForAJobDescription}</small> : null}
                <p>My contacts: </p>
                <Contacts contacts={props.profile.contacts}/>
            </div>
        </div>
    )
};

export default Profile;