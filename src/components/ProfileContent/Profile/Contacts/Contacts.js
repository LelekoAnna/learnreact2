import React from "react";
import "./Contacts.css"

const Contacts = (props) => {
    let contacts = [];
    for (let contact in props.contacts) {
        if (props.contacts[contact]) {
            contacts.push({id: contacts.length+1, network: contact, address: props.contacts[contact]})
        }
    }
    let contactElement = contacts.map(contact => <p><a key={contact.id} href={contact.address} className='contact'>{contact.network}</a></p>)

    return (
        <>
            {contactElement}
        </>
    )
};

export default Contacts;