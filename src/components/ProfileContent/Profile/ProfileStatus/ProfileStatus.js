import React, { useEffect, useState } from "react";

const ProfileStatus = (props) => {
    let [editMode, setEditMode] = useState(false);
    let [status, setStatus] = useState(props.status);

    useEffect(() => {
        setStatus(props.status)
    }, [props.status]);

    let onEditMode = () => {
        setEditMode(true);
    };

    let offEditMode = () => {
        setEditMode(false);
        props.updateStatus(status);
    };

    let editStatus = (event) => {
        setStatus(event.currentTarget.value);
    };

    return (
        <div>
            {!editMode ?
                <span onClick={onEditMode}>Status: {props.status || 'No status'}</span> :
                <input autoFocus={true} onBlur={offEditMode} value={status} onChange={editStatus} />
            }
        </div>
    )
}

export default ProfileStatus;