import React from "react";
import './ProfileContent.css'

import Profile from "./Profile/Profile";
import MyPostsContainer from "./MyPosts/Posts/MyPostsContainer";


const ProfileContent = (props) => {
    return (
        <div>
            <img className="content__banner"
                 src="https://images.pexels.com/photos/1080722/pexels-photo-1080722.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260"
                 alt="banner"/>
            <Profile profile={props.profile} status={props.status} updateStatus={props.updateStatus}/>
            <MyPostsContainer />
        </div>
    )
};

export default ProfileContent;