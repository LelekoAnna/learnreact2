import React from "react";
import './NewPost.css'

const NewPost = (props) => {
    let onClickSendPost = () => {
        props.addNewPost();
    };

    let onChangeText = (event) => {
        props.createNewText(event.target.value);
    };

    return (
        <div className="newPost">
            <textarea value={props.newText} onChange={onChangeText}  cols="100"
                      rows="3"/>
            <button className='button' onClick={onClickSendPost}>Send</button>
        </div>
    )
};

export default NewPost;
