import React from "react";
import './Post.css'

const Post = (props) => {
    return (
        <div>
            <div>{props.message}</div>
            <span className="likes">like: {props.likes}</span>
        </div>
    )
};

export default Post;