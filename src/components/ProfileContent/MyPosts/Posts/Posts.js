import React from "react";
import './Posts.css'

import Post from "./Post/Post";

const Posts = (props) => {
    let posts = props.postsData.map(post => <Post key={post.id} message={post.text} likes={post.likesCount}/>)
    return (
        <div className="posts">
            {posts}
        </div>
    )
};

export default Posts;
