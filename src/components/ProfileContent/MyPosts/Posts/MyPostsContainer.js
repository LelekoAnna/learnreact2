import React from "react";

import MyPosts from "../MyPosts";
import {addPostActionCreator, createNewPostActionCreator} from "../../../../redux/profile-reducer";
import {connect} from "react-redux";

const mapStateToProps = (state) => {
    return {
        posts: state.profilePage.postsData,
        newText: state.profilePage.newText
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addNewPost: () => {dispatch(addPostActionCreator())},
        createNewText: (text) => {dispatch(createNewPostActionCreator(text))}
    }
};

const MyPostsContainer = connect(mapStateToProps, mapDispatchToProps)(MyPosts);

export default MyPostsContainer;