import React from "react";

import './MyPosts.css'
import Posts from "./Posts/Posts";
import NewPost from "./NewPost/NewPost";

const MyPosts = (props) => {
    return (
        <div className='MyPosts'>
            <h3>My posts</h3>
            <NewPost createNewText={props.createNewText} addNewPost={props.addNewPost} newText={props.newText} />
            <Posts postsData={props.posts}/>
        </div>
    )
};

export default MyPosts;
