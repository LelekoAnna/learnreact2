import React from "react";
import ProfileContent from "./ProfileContent";
import {connect} from "react-redux";
import {
    getUserStatusThunkCreator,
    setUserProfileThunkCreator,
    updateUserStatusThunkCreator
} from "../../redux/profile-reducer";
import {withRouter} from "react-router";
import {AuthRedirect} from "../../hoc/AuthRedirect";
import {compose} from "redux";

class ProfileContainer extends React.Component {

    componentDidMount() {
        let userId = this.props.match.params.userId || this.props.userId;
                this.props.setUserProfile(userId);
                this.props.getUserStatus(userId);
    }

    render() {
        return <ProfileContent
            profile={this.props.profile}
            status={this.props.status}
            updateStatus={this.props.updateUserStatus}
        />
    }
}

let mapStateToProps = (state) => ({
    profile: state.profilePage.userProfile,
    userId: state.auth.id,
    status: state.profilePage.status
});

let mapDispatchToProps = (dispatch) => ({
    setUserProfile: (userId) => {
        dispatch(setUserProfileThunkCreator(userId))
    },
    getUserStatus: (userId) => {
        dispatch(getUserStatusThunkCreator(userId))
    },
    updateUserStatus: (status) => {
        dispatch(updateUserStatusThunkCreator(status))
    }
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    withRouter,
    AuthRedirect
)(ProfileContainer)

