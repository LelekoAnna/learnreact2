import React from "react";
import "./Login.css"
import {useFormik} from "formik";
import {connect} from "react-redux";
import {logInThunkCreator} from "../../redux/auth-reducer";
import {validate} from "../../utils/validators/formsValidators";
import {Redirect} from "react-router-dom";

const Login = (props) => {
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            checked: false
        },
        validate,
        onSubmit: values => {
            props.login(values.email, values.password, values.checked);
        },
    });

    if(props.isAuth) {
        return <Redirect to='/profile'/>
    }

    return (
        <div className='login'>
            <p className='login__label'>Sign in</p>
            <form onSubmit={formik.handleSubmit}>
                <label htmlFor="email"> Email:
                    <input
                        id='email'
                        className='login__input'
                        placeholder='Enter your email'
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    {formik.touched.email && formik.errors.email ? <div className='error'>{formik.errors.email}</div> : null}
                </label>
                <label htmlFor="password"> Password:
                    <input
                        id='password'
                        className='login__input'
                        type="password"
                        placeholder='Enter your password'
                        value={formik.values.password}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                    />
                    {formik.touched.password && formik.errors.password ? <div className='error'>{formik.errors.password}</div> : null}
                </label>
                <label htmlFor="checked">
                    <input
                        id='checked'
                        type="checkbox"
                        value={formik.values.checked}
                        onChange={formik.handleChange}
                    /> remember me
                </label>
                <button disabled={!formik.isValid} className='login__button'>Login</button>
            </form>
        </div>
    )
};
const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth
});
const mapDispatchToProps = (dispatch) => ({
    login: (email, password, checked) => {
        dispatch(logInThunkCreator(email, password, checked))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);