import {NavLink} from "react-router-dom";
import React from "react";
import './Interlocutor.css'

const Interlocutor = (props) => {
    let path = '/messages/' + props.id;
    return (
        <div className="interlocutor">
            <NavLink to={path}>{props.name}</NavLink>
        </div>
    )
}

export default Interlocutor;