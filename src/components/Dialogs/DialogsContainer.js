import React from "react";
import {connect} from "react-redux";

import Dialogs from "./Dialogs";
import {addMessageActionCreator, createNewMessageActionCreator} from "../../redux/dialogs-reducer";
import {AuthRedirect} from "../../hoc/AuthRedirect";
import {compose} from "redux";

const mapStateToProps = (state) => {
    return {
        dialogsPage: state.dialogsPage,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addNewMessage: () => {
            dispatch(addMessageActionCreator())
        },
        createNewText: (text) => {
            dispatch(createNewMessageActionCreator(text))
        }
    }
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    AuthRedirect
)(Dialogs);
