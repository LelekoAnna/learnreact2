import React from "react";

const Dialogues = (props) => {
    return (
        <div className="dialog">{props.message}</div>
    )
}

export default Dialogues;