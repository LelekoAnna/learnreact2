import React from "react";
import './Dialogs.css'

import Dialogues from "./Dialogues/Dialogues";
import Interlocutor from "./Interlocutor/Interlocutor";
import {Redirect} from "react-router-dom";

const Dialogs = (props) => {
    let interlocutors = props.dialogsPage.interlocutorsData.map((user, index) => <Interlocutor key={index} name={user.name} id={user.id}/>)
    let dialogues = props.dialogsPage.dialoguesData.map((dialog, index) => <Dialogues key={index} message={dialog.text}/>)

    let onChangeText = (event) => {
        props.createNewText(event.target.value);
    };

    let onClickSendMessage = () => {
        props.addNewMessage()
    };

    return (
        <div className='messages'>
            <div className="interlocutors">
                <h4>Interlocutors:</h4>
                {interlocutors}
            </div>
            <div className="dialogues">
                {dialogues}
                <div className="newMessage">
                    <textarea onChange={onChangeText} value={props.dialogsPage.newText} cols="100" rows="3"/>
                    <button className='button' onClick={onClickSendMessage}>Send</button>
                </div>
            </div>
        </div>
    )
};

export default Dialogs;
