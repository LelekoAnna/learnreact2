import React from "react";
import "./User.css";
import userPhoto from "../../../assets/images/default-user-image.png"
import {NavLink} from "react-router-dom";

const User = (props) => {
    return (
        <div className='userCard'>
            <div>
                <NavLink to={'/profile/' + props.user.id}>
                    <img className='userCard__userPhoto'
                         src={props.user.photos.small !== null ? props.user.photos.small : userPhoto}
                         alt="user photo"/>
                </NavLink>
            </div>
            <div className='userCard__userInfo'>
                <h3>{props.user.name}</h3>
                <p>Status: {props.user.status !== null ? props.user.status : 'I have not status :('}</p>
            </div>
            <div className='friendshipButton'>
                <button
                    onClick={() => {
                       props.setFriendship(props.user.id)
                    }}>{props.user.followed ? 'Unfollow' : 'Follow'}</button>
            </div>
        </div>
    )
};

export default User;
