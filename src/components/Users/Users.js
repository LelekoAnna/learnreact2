import React from "react";
import User from "./User/User";
import "./Users.css"

const Users = (props) => {
    let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);
    let pages = [];
    if (pagesCount <= 7) {
        for (let i = 1; i <= pagesCount; i++) {
            pages.push(i);
        }
    } else if (props.currentPage < 5) {
        pages = [ 1, 2, 3, 4, 5, '...', pagesCount ]
    } else if (props.currentPage > pagesCount - 5) {
        pages = [ 1, '...', pagesCount - 5, pagesCount - 4, pagesCount - 3, pagesCount - 2, pagesCount - 1, pagesCount ]
    } else {
        pages = [ 1, '...', props.currentPage - 1, props.currentPage, props.currentPage + 1, '...', pagesCount ]
    }

    return (
        <div>

            <div>
                <button onClick={() => {props.onPageChanged(props.currentPage-1)}}>Prev</button>
                {pages.map(page => <button
                    key={page}
                    className={props.currentPage === page ? 'activePage' : null}
                    onClick={() => {props.onPageChanged(page)}}>{page}</button>)}
                <button onClick={() => {props.onPageChanged(props.currentPage+1)}}>Next</button>
            </div>
            <h2>Users:</h2>
            <div>
                {props.users.map(user => <User key={user.id} user={user} setFriendship={props.setFriendship}/>)}
            </div>
        </div>
    )
};

export default Users;
