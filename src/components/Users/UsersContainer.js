import React from "react";
import {connect} from "react-redux";
import Users from "./Users";
import {
    getUsersThunkCreator,
    setCurrentPageAC,
    setFriendshipThunkCreator,
} from "../../redux/users-reducer";
import {AuthRedirect} from "../../hoc/AuthRedirect";
import {compose} from "redux";

class UsersContainer extends React.Component {
    componentDidMount() {
        this.props.getUsers(this.props.currentPage, this.props.pageSize);
    }

    onPageChanged = (page) => {
        this.props.setCurrentPage(page);
        this.props.getUsers(page, this.props.pageSize);
    };

    render() {
        return (
            <>
                {this.props.isFetching ? <p>Wait a second ...</p> : <Users
                    users={this.props.users}
                    setFriendship={this.props.setFriendship}
                    totalUsersCount={this.props.totalUsersCount}
                    pageSize={this.props.pageSize}
                    currentPage={this.props.currentPage}
                    onPageChanged={this.onPageChanged}/>}
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    users: state.usersPage.users,
    pageSize: state.usersPage.pageSize,
    totalUsersCount: state.usersPage.totalItemsCount,
    currentPage: state.usersPage.currentPage,
    isFetching: state.usersPage.isFetching
});

const mapDispatchToProps = (dispatch) => ({
    setFriendship: (userId) => {
        dispatch(setFriendshipThunkCreator(userId))
    },
    setCurrentPage: (currentPage) => {
        dispatch(setCurrentPageAC(currentPage))
    },
    getUsers: (currentPage, pageSize) => dispatch(getUsersThunkCreator(currentPage, pageSize))
});

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    AuthRedirect
)(UsersContainer);
