const ADD_MESSAGE = 'dialogs/ADD-MESSAGE';
const CREATE_NEW_MESSAGE = 'dialogs/CREATE-NEW-MESSAGE';

let initialState = {
    interlocutorsData: [
        {id: 1, name: 'Anna'},
        {id: 2, name: 'Natali'},
        {id: 3, name: 'Dima'},
        {id: 4, name: 'Slava'},
        {id: 5, name: 'Andrey'}
    ],
    dialoguesData: [
        {id: 1, text: 'Hello!'},
        {id: 2, text: 'Its me!'},
        {id: 3, text: 'Hello!'},
        {id: 4, text: 'How are you?'},
        {id: 5, text: 'Do you want go to the cinema?'},
        {id: 6, text: 'oh! yes!!'}
    ],
    newText: ''
};

const dialogsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MESSAGE:
            let newMessage = {
                id: state.dialoguesData.length + 1,
                text: state.newText
            }
            return  {...state, dialoguesData: [...state.dialoguesData, newMessage], newText: ''};
        case CREATE_NEW_MESSAGE:
            return  {...state, newText: action.text};
        default: return state;
    }
};

export const addMessageActionCreator = () => ({type: ADD_MESSAGE});
export const createNewMessageActionCreator = (text) => ({type: CREATE_NEW_MESSAGE, text: text})

export default dialogsReducer;