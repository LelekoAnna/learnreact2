import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";

let store = {
    _state: {
        profile: {
            postsData: [
                {id: 1, text: 'Hello! How are you?', likesCount: 12},
                {id: 2, text: 'Great! Thanks! And you?', likesCount: 11}
            ],
            newText: ''
        },
        dialogs: {
            interlocutorsData: [
                {id: 1, name: 'Anna'},
                {id: 2, name: 'Natali'},
                {id: 3, name: 'Dima'},
                {id: 4, name: 'Slava'},
                {id: 5, name: 'Andrey'}
            ],
            dialoguesData: [
                {id: 1, text: 'Hello!'},
                {id: 2, text: 'Its me!'},
                {id: 3, text: 'Hello!'},
                {id: 4, text: 'How are you?'},
                {id: 5, text: 'Do you want go to the cinema?'},
                {id: 6, text: 'oh! yes!!'}
            ],
            newText: ''
        }
    },
    _callSubscriber() {
        console.log('rerender')
    },
    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer
    },
    dispatch(action) {
        this._state.profile = profileReducer(this._state.profile, action);
        this._state.dialogs = dialogsReducer(this._state.dialogs, action);
        this._callSubscriber(this._state);
    }
}

export default store;