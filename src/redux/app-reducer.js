import { setAuthUserDataThunkCreator } from "./auth-reducer";

const SET_INITIALIZED = 'SET_INITIALIZED'

let initialState = {
    initialize: false
}

const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_INITIALIZED:
            return { ...state, initialize: true };
        default: return state;
    }
}

export const initializedAC = () => {
    return {
        type: SET_INITIALIZED
    }
};

export const initializeApp = () => (dispatch) => {
    let promise = dispatch(setAuthUserDataThunkCreator());
    promise.then(() => {
        dispatch(initializedAC())
    })
}

export default appReducer;