import {usersAPI} from "../api/api";

const SET_FRIENDSHIP = 'users/SET-FRIENDSHIP';
const SET_USERS = 'users/SET-USERS';
const SET_CURRENT_PAGE = 'users/SET-CURRENT-PAGE';
const SET_ITEMS_TOTAL_COUNT = 'users/SET-USERS-TOTAL-COUNT';
const TOGGLE_IS_FETCHING = 'users/TOGGLE_IS_FETCHING';

let initialState = {
    users: [],
    pageSize: 10,
    totalItemsCount: 0,
    currentPage: 1,
    isFetching: false
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USERS:
            return {...state, users: action.users};
        case SET_FRIENDSHIP:
            return {
                ...state,
                users: state.users.map(user => user.id === action.userId ? {...user, followed: !user.followed} : user)
            };
        case TOGGLE_IS_FETCHING:
            return {...state, isFetching: action.status};
        case SET_CURRENT_PAGE:
            return  {...state, currentPage: action.currentPage}
        case SET_ITEMS_TOTAL_COUNT:
            return {...state, totalItemsCount: action.totalCount}
        default:
            return state;
    }
};

export const setFriendshipAC = (userId) => ({type: SET_FRIENDSHIP, userId});
export const setUsersAC = (users) => ({type: SET_USERS, users});
export const toggleFetchingAC = (status) => ({type: TOGGLE_IS_FETCHING, status});
export const setCurrentPageAC = (currentPage) => ({type: SET_CURRENT_PAGE, currentPage});
export const setItemsTotalCountAC = (totalCount) => ({type:SET_ITEMS_TOTAL_COUNT, totalCount});

export const getUsersThunkCreator = (currentPage, pageSize) => (dispatch) => {
    dispatch(toggleFetchingAC(true));
    usersAPI.getUsers(currentPage, pageSize).then(data => {
        dispatch(setUsersAC(data.items));
        dispatch(setItemsTotalCountAC(data.totalCount));
        dispatch(toggleFetchingAC(false));
    })
};

export const setFriendshipThunkCreator = (userId) => (dispatch) => {
    usersAPI.isFriend(userId)
        .then(data => {
            if (data) {
                usersAPI.unFollow(userId)
                    .then(data => {
                        if (data.resultCode === 0) {
                            dispatch(setFriendshipAC(userId))
                        }
                    })
            } else {
                usersAPI.follow(userId)
                    .then(data => {
                        if (data.resultCode === 0) {
                            dispatch(setFriendshipAC(userId))
                        }
                    })
            }
        });
};

export default usersReducer;
