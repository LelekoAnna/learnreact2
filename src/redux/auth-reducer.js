import {loginAPI} from "../api/api";

const SET_USER_DATA = 'auth/SET-USER-DATA';

let initialState = {
    isAuth: false,
    id: null,
    email: '',
    login: ''
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return {...state, ...action.data };
        default:
            return state
    }
};

export const setAuthUserDataAC = (id, email, login, isAuth) => ({type: SET_USER_DATA, data: {id, email, login, isAuth}});

export const setAuthUserDataThunkCreator = () => (dispatch) => {
    return loginAPI.authMe().then(data => {
        if (data.resultCode === 0) {
            let {id, email, login} = data.data;
            dispatch(setAuthUserDataAC(id, email, login, true))
        }
    })
};

export const logInThunkCreator = (email, password, checked) => (dispatch) => {
    loginAPI.signIn({email, password, checked: checked || false}).then(data => {
        if(data.resultCode === 0) {
            dispatch(setAuthUserDataThunkCreator())
        } else {
            alert('Email or password is wrong')
        }
    })
}

export const logOutThunkCreator = () => (dispatch) => {
    loginAPI.signOut().then(data => {
        if (data.resultCode === 0) {
            dispatch(setAuthUserDataAC(null, '', '', false))
        }
    })
}

export default authReducer;
