import {profileAPI} from "../api/api";

const ADD_POST = 'profile/ADD-POST';
const CREATE_NEW_POST = 'profile/CREATE-NEW-POST';
const SET_USER_PROFILE = 'profile/SET-USER-PROFILE';
const SET_USER_STATUS = 'profile/SET-USER-STATUS'

let initialState = {
    postsData: [
        {id: 1, text: 'Hello! How are you?', likesCount: 12},
        {id: 2, text: 'Great! Thanks! And you?', likesCount: 11}
    ],
    newText: '',
    userProfile: {
        userId: 0,
        lookingForAJob: false,
        lookingForAJobDescription: '',
        fullName: '',
        contacts: {
            github: '',
            vk: '',
            facebook: '',
            instagram: '',
            twitter: '',
            website: '',
            youtube: '',
            mainLink: ''
        },
        photos: {
            small: '',
            large: ''
        }
    },
    status: ''
};

const profileReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST:
            let newPost = {
                id: state.postsData.length + 1,
                text: state.newText,
                likesCount: 0
            };
            return {...state, postsData: [...state.postsData, newPost], newText: ''};
        case CREATE_NEW_POST:
            return {...state, newText: action.text};
        case SET_USER_PROFILE:
            return {...state, userProfile: action.profile};
        case SET_USER_STATUS:
            return {...state, status: action.status};
        default:
            return state;
    }
};

export const addPostActionCreator = () => ({type: ADD_POST});
export const createNewPostActionCreator = (text) => ({type: CREATE_NEW_POST, text: text});
export const setUserProfileAC = (profile) => ({type: SET_USER_PROFILE, profile});
export const setUserStatusAC = (status) => ({type: SET_USER_STATUS, status});

export const setUserProfileThunkCreator = (userId) => (dispatch) => {
    profileAPI.getUserProfile(userId)
        .then(data => {
            dispatch(setUserProfileAC(data));
        });
};

export const getUserStatusThunkCreator = (userId) => (dispatch) => {
    profileAPI.getUserStatus(userId).then(data => {
        dispatch(setUserStatusAC(data))
    })
}

export const updateUserStatusThunkCreator = (status) => (dispatch) => {
    profileAPI.updateStatus(status).then(data => {
        if(data.resultCode === 0) {
            dispatch(setUserStatusAC(status))
        }
    })
}

export default profileReducer;
